#!/usr/bin/env python
# coding: utf-8

# # Object Factory

# In[1]:


import numpy as np
import yaml
from collections import namedtuple


# 
# We can define scenes by specifying object types and their parameters. We will generate all object type definitions from setting default parameters 

# In[2]:


DefaultCamera = {
    'position': [0,0,0],
    'yaw': 0.0,
    'pitch': 0.0, 
    'width': 300,
    'height': 200,
    'field_of_view': 60.0,
    'focal_length': .1,
}


# In[3]:


DefaultSphere = {
    'center': [0,0,0], 
    'radius': 1.0, 
    'color': [1,1,1], 
    'shininess': .5,
    'alpha': 1.0,
    'refractive_index': 1.0,
}


# In[4]:


DefaultPlane = {
    'support': [0,0,0], 
    'normal': [0,1,0], 
    'color': [1,1,1], 
    'shininess': 0.5,
    'alpha': 1.0,
    'refractive_index': 1.0,
}


# In[5]:


DefaultPointLight = {
    'position': [0,0,0], 
    'ambient': [1,1,1,.3], 
    'diffuse': [1,1,1,.4], 
    'specular': [1,1,1,.3], 
    'brightness': 1.0
}


# Now generate the named tuples

# In[6]:


all_object_types = []

for var in dir():
    if var.startswith("Default"):
        varname, var = var[7:], eval(var)
        params = [param for param in var]
        defaults = [np.asarray(var[param]) for param in params]
        
        #generate tuple
        var = namedtuple(varname, params, defaults=defaults)
        exec('%s = var' % varname)
        all_object_types.append(var)
all_object_types = tuple(all_object_types)


# # Scene  Loader 
# 
# It is very convenient to define the scenes in YAML files and use this routine to load them

# In[7]:


def parse_scene(path):
    
    #use numerical data from YAML sheets
    with open(path) as f:
        description = yaml.safe_load(f)
    
    #iteratively grow scene tree 
    scene = dict()
    for name in description:
        
        assert 'object' in description[name], "Object '%s' has no object type." % name
        obj = description[name]['object']
        
        #Load parameters from dictionary
        params = description[name]
        params.pop('object')
        params = {param: np.asarray(params[param]) 
                  for param in params}
        #TODO: cast dtypes from Defaults 
        
        #Create scene objects 
        obj_type = eval(obj)
        scene[name] = obj_type(**params)
    
    return scene


# In[ ]:




