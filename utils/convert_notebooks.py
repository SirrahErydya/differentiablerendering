#!/usr/bin/env python3

import os

if __name__ == '__main__':
    notebooks = [
        f.replace(' ', '\\ ')
        for f in os.listdir('..')
        if os.path.splitext(f)[1] == '.ipynb'
    ]
    
    for notebook in notebooks:
        cmd = "jupyter nbconvert ../%s --to html" % notebook
        os.system(cmd)