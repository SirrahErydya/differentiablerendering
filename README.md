# DifferentiableRendering

A Differentiable Raytracing Renderer allows using it as a layer in a Deep Neural Network.   

The project consists of three major parts:


1. Implementing a General ray-tracing rendering algorithm
2. Enforce differentiabilty, in particular use stochastic rays
3. Port from algorithms implemented in NumPy to tensorflow and parallelize 
4. Attach Neural Networks for prediction tasks
    
Currently the project is under construction.